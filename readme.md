# RiggingTools

RiggingTools is an add-on for Blender regrouping some little tools I made for myself to automate redundant tasks (isn't that what programs are made for in the first place ?).


# List of tools

**Reset Stretch To**
In pose mode, used for reseting every "Stretch To" constraint in the armature.

**Set Inverse Child Of**  
In pose mode, used for setting the inverse of every "Child Of" constraint in the armature.
